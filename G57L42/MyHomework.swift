//
//  MyHomework.swift
//  G57L42
//
//  Created by Ars on 10.04.2018.
//  Copyright © 2018 ArsenIT. All rights reserved.
//

import UIKit

class MyHomework: NSObject {
    //Task 1:
    static func helloWorld() {
        print("Hello world!")
    }
    
    //Task 2:
    static func helloSeveralTimes(helloTimes: Int) {
        for _ in 0..<helloTimes {
            helloWorld()
        }
    }
    //Task 3:
    static func dayOfTheWeek(numberOfTheDay: Int) {
        
        switch numberOfTheDay {
        case 1:
            print("Monday")
        case 2 :
            print("Tuesday")
        case 3:
            print("Wednesday")
        case 4:
            print("Thursday")
        case 5:
            print("Friday")
        case 6:
            print("Saturday")
        case 7:
            print("Sunday")
        default:
            print("Net takogo dnya")
        }
        
    }
    
    //Task 4:
    static func task3Reincarnated(count: Int) {
        var dayOfTheWeek = 0
        
        if count > 7 {
            dayOfTheWeek = count%7
            if count%7 == 0{
                dayOfTheWeek = 7
            }
        } else if count%7 <= 7 {
            dayOfTheWeek = count
        } else if count%7 == 0 {
            dayOfTheWeek = 7
        }
        
        switch dayOfTheWeek {
        case 1:
            print("Monday")
        case 2 :
            print("Tuesday")
        case 3:
            print("Wednesday")
        case 4:
            print("Thursday")
        case 5:
            print("Friday")
        case 6:
            print("Saturday")
        case 7:
            print("Sunday")
        default:
            print("Net takogo dnya")
        }
        
    }
    
    //Task 5:
    static func fibonachi(number: Int) {
        var fib0 = 0
        var fib1 = 1
        var fib = 0
        
        for i in 2..<number {
            fib = fib1 + fib0
            fib0 = fib1
            fib1 = fib
        }
        if number == 0 {
            fib1 = 0
        }
        print("Number Fibonacchi = \(fib1)")
    }
    
    //Task 6:
    static func cmToInches(count: Double){
        let inch = 2.54
        print("\(count) cantimeters = \(inch * count) inches")
    }
    
    //Task 7:
    static func completeSong(howManyLines: Int){
        for i in 0..<(howManyLines + 1) {
            let x = howManyLines - i
            print("\(x) bottles of beer on the wall, \(x) bottles of beer.")
            print("Take one down and pass it around, \(x) bottles of beer on the wall")
        }
    }
    
    //Task 8:
    static func integerDivide(number: Int){
        print("Числа которые можноо нацело делить \(number) в рамках 1..100")
        for i in 1..<101 {
            if i%number == 0{
                print(i)
            }
        }
        
    }
    
}
